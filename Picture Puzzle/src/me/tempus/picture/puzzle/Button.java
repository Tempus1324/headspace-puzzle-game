/**
 * 
 */
package me.tempus.picture.puzzle;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

/**
 * @author Chris
 *
 */
public abstract class Button implements Drawable, AABB{

	private final int[] textures = new int[2];
	private float x;
	private float y;
	private int width;
	private int height;
	private Rectangle rect;
	
	protected int state = 0;

	/**
	 * @param normalID
	 * @param x
	 * @param y
	 * @param width
	 * @param height
	 */
	public Button(int normalID, int pressedID, float x, float y, int width, int height) {
		this.textures[0] = normalID;
		this.textures[1] = pressedID;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.rect = new Rectangle((int)x, (int)y, width, height);
	}

	public abstract void pushed();

	public abstract void released();

	public void preDraw(GL10 gl) {

	}

	public void draw(GL10 gl) {
		
		Render.bindTexture(gl, GL11.GL_TEXTURE_2D, textures[state]);

		((GL11Ext) gl).glDrawTexfOES(x, y, 1, width, height);
	}

	public void postDraw(GL10 gl) {

	}

	public Rectangle getRect() {
		rect.x = x;
		rect.y = y;
		return rect;
	}

}
