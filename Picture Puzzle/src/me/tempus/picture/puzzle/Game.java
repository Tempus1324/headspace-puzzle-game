package me.tempus.picture.puzzle;

import android.support.v4.app.Fragment;


public class Game implements Runnable {

	public boolean done = false;

	private static boolean changingScreen;

	public static Render render;

	private static MainActivity main;
	
	public static Screen currentScreen;

	public final static Screen[] screens = new Screen[2];
	
	public Game(MainActivity main, Render render){
		Game.main = main;
		Game.render = render;
	}

	private void init() {
		
	}

	public void pause(){
		done = true;
		if(currentScreen != null)
			currentScreen.pause(this);
	}

	public void resume(){
		if(currentScreen != null)
			currentScreen.resume(this, render);
	}

	public void run() {
		init();
		screens[0] = new MainMenu();
		changeScreen(0);
		currentScreen.init(this, render);
		while(!done){
			currentScreen.preUpdate(this, render);
			InputManager.processInput(this, render);
			update();			
			
		}

	}

	public void update(){
		currentScreen.update(this, render);
	}
	
	public static void changeScreen(int screenIndex){
		assert (screenIndex > -1);
		changingScreen = true;
		
		main.fragTranscation = main.fragManager.beginTransaction();
		main.fragTranscation.replace(me.tempus.picture.puzzle.R.id.viewMain, (Fragment) screens[screenIndex]);
		main.fragTranscation.addToBackStack(screens[screenIndex].toString());
		main.fragTranscation.commit();
		
		currentScreen = screens[screenIndex];
		
		
		changingScreen = false;
	}

	public void waitThis(){
		try{
			synchronized(this){
				this.wait();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void notifyThis(){
		try{
			synchronized(this){
				this.notify();
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}

}
