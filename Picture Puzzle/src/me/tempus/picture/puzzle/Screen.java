/**
 * 
 */
package me.tempus.picture.puzzle;

import android.view.KeyEvent;
import android.view.MotionEvent;

/**
 * @author Chris
 *
 */
public interface Screen {

	/**
	 * Used for things to setup before the input is processed
	 * @param game
	 * @param render
	 */
	public abstract void preUpdate(Game game, Render render);
	
	public abstract void update(Game game, Render render);
	
	public abstract void pause(Game game);
	
	public abstract void init(Game game, Render render);
	
	public abstract void resume(Game game, Render render);
	
	public abstract void stop(Game game);
	
	public abstract boolean keyPressed( KeyEvent event);
	
	public abstract boolean onTouch(MotionEvent event);
}
