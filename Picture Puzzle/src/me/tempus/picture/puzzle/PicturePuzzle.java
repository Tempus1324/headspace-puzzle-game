package me.tempus.picture.puzzle;

import java.util.Random;

import me.tempus.picture.puzzle.R;
import me.tempus.picture.puzzle.WinGameDialog.WinGameListener;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class PicturePuzzle extends Fragment implements Screen, WinGameListener, Parcelable {

	private static final String timeFormat = "00:00:00";

	public static final Piece freePiece = new Piece(0, 0, new int[] { 0, 0, 0,
			0 });

	public static final Piece[] activePieces = new Piece[4];

	public static int rowSize = 3;
	public static int columnSize = 3;

	private static boolean pieceSelected = false;
	private static Piece piece = null;
	public static Piece[] pieces;

	private TextView timeTextField;
	private long upTime = 0;
	private long startTime;
	private final StringBuffer timeTextBuffer = new StringBuffer(timeFormat);
	private GLSurfaceView glSurfaceView;

	private static Runnable setText;

	private Render render;

	private Handler handler;

	public static boolean ready = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		View view = inflater.inflate(R.layout.activity_game, container, false);
		timeTextField = (TextView) view.findViewById(R.id.timeTextField);
		setUpButtons(view);

		glSurfaceView = (GLSurfaceView) view.findViewById(R.id.glView);
		glSurfaceView.setFocusable(true);
		glSurfaceView.setOnClickListener(MainActivity.selfInstance);
		glSurfaceView.setOnTouchListener(MainActivity.selfInstance);
		glSurfaceView.setOnKeyListener(MainActivity.selfInstance);

		render = new Render(container.getContext());
		glSurfaceView.setRenderer(render);

		startTime = SystemClock.uptimeMillis();
		setText = new Runnable() {

			@Override
			public void run() {
				  upTime = SystemClock.uptimeMillis() - startTime;
				  timeTextBuffer.delete(0, timeTextBuffer.length());
				  timeTextBuffer.append((upTime/(1000*60*60)%60)).append(":")
				  .append((int)(upTime/(1000*60)%60)).append(":")
				  .append((int)(upTime/1000)%60); //Seconds
				  
				  timeTextField.setText(timeTextBuffer);
				 
				 handler.postDelayed(this, 1000);
				 
			}
		};

		return view;
	}

	@Override
	public void onResume() {
		handler = new Handler();
		handler.removeCallbacks(setText);
		handler.postDelayed(setText, 100);
		render.notifyThis();
		super.onResume();
	}

	@Override
	public void onPause() {
		render.notifyThis();
		handler.removeCallbacks(setText);
		super.onPause();
	}

	@Override
	public void onDestroyView() {
		destroySelf();
		super.onDestroyView();
	}

	/**
	 * Method to set all the references and variables to null Helps the GC clean
	 * up
	 */
	public void destroySelf() {
		
		render = null;
		glSurfaceView = null;
		piece = null;
		pieces = null;
		freePiece.i = 0;
		freePiece.j = 0;
		activePieces[0] = null;
		activePieces[1] = null;
		activePieces[2] = null;
		activePieces[3] = null;
		ready = false;
		timeTextField = null;
		upTime = 0;
		startTime = 0;
	}

	/**
	 * Method to handle moving back to the main menu
	 */
	private void moveBack() {
		handler.removeCallbacks(setText);
		MainActivity.selfInstance.fragManager.popBackStack();
		Game.currentScreen = Game.screens[0];
	}

	public void setUpButtons(View view) {

		final Button backButton = (Button) view.findViewById(R.id.backButton);

		backButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				moveBack();
			}
		});

		final Button resetButton = (Button) view
				.findViewById(R.id.resestButton);

		resetButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				PicturePuzzle.shuffPieces(pieces);
				startTime = SystemClock.uptimeMillis();
			}
		});
	}

	public void preUpdate(Game game, Render render) {
		if (!ready)
			return;
		activePieces[0] = getActivePiece(freePiece.i, freePiece.j + 1);
		activePieces[1] = getActivePiece(freePiece.i, freePiece.j - 1);
		activePieces[2] = getActivePiece(freePiece.i + 1, freePiece.j);
		activePieces[3] = getActivePiece(freePiece.i - 1, freePiece.j);
	}

	public void update(Game game, Render render) {
		if (!ready)
			return;

		this.render.addDrawables(pieces);
		InputManager.frameDone();
		if (render != null)
			render.beginFrame();
	}

	public void pause(Game game) {
	}

	public void init(Game game, Render render) {
	}

	public void resume(Game game, Render render) {
	}

	public void stop(Game game) {
	}

	/**
	 * Created to prevent array out of bounds when getting the different active
	 * pieces
	 * 
	 * @param i
	 * @param j
	 * @return The active piece
	 */
	public Piece getActivePiece(int i, int j) {
		if(i < 0 || j < 0) return null;
		final int pos = getPositionInArray(i, j, rowSize);
		if (pieces == null)
			return null;
		if (pos >= 0 && pos < pieces.length) {

			return pieces[pos];
		}
		return null;

	}

	/**
	 * Sets up the grid of pieces for the image
	 * 
	 * @param textureWidth
	 * @param textureHeight
	 */
	public static void setUpGrid(int rowPieces, int columnPieces,
			int textureWidth, int textureHeight) {
		pieces = new Piece[rowPieces * columnPieces];
		Piece.pieceWidth = Render.screenWidth / rowPieces;
		Piece.pieceHeight = Render.screenHeight / columnPieces;
		Piece.xPadding = 2;
		Piece.yPadding = 2;
		final int tPieceWidth = textureWidth / (rowPieces);
		final int tPieceHeight = textureHeight / (columnPieces);
		for (int i = 0; i < rowPieces; i++) {
			for (int j = 0; j < columnPieces; j++) {
				final int position = getPositionInArray(i, j, rowPieces);
				final Piece piece = new Piece(i, j, new int[] {
						i * tPieceWidth, textureHeight - (j * tPieceHeight),
						tPieceWidth, -tPieceHeight });
				pieces[position] = piece;
			}
		}
		pieces[getPositionInArray(0, 0, rowPieces)] = null;
		freePiece.getRect().height = Piece.pieceHeight * 2;
		freePiece.getRect().width = Piece.pieceWidth * 2;

		PicturePuzzle.shuffPieces(pieces);
	}

	/**
	 * To check if the pieces are in the correct places The method will take
	 * care of the case when the game is won
	 * 
	 * @param piece Piece array of the current state of the pieces
	 */
	private void checkIfComplete(Piece[] pieces) {
		Piece currentPiece = null;
		for (int i = 0; i < pieces.length; i++) {
			currentPiece = pieces[i];
			if (currentPiece != null) {
				if (getPositionInArray(currentPiece.initI, currentPiece.initJ,
						rowSize) != i) {
					return;
				}
			}
		}
		handler.removeCallbacks(setText);
		showWinGameDialog();
	}

	/**
	 * 
	 * @param piece
	 *            The current Piece to move
	 * @param i
	 *            Row position to move to
	 * @param j
	 *            Column position to move to
	 */
	public void movePieceTo(Piece piece, int i, int j) {
		final int freePos = getPositionInArray(freePiece.i, freePiece.j,
				rowSize);
		final int posTry = getPositionInArray(i, j, rowSize);
		if (posTry == freePos) {
			swapPiece(piece, freePiece);
			checkIfComplete(pieces);
		}
	}

	/**
	 * Swap two pieces positions
	 * 
	 * @param p1
	 *            Piece one
	 * @param p2
	 *            Piece two
	 */
	public static void swapPiece(Piece p1, Piece p2) {
		if(p1 == null || p2 == null) return;
		
		final int pos1 = getPositionInArray(p1.i, p1.j, rowSize);
		final int pos2 = getPositionInArray(p2.i, p2.j, rowSize);
		final Piece p = pieces[pos1];
		pieces[pos1] = pieces[pos2];
		pieces[pos2] = p;
		final int i1 = p1.i;
		final int j1 = p1.j;
		p1.i = p2.i;
		p1.j = p2.j;
		p2.i = i1;
		p2.j = j1;

	}

	/**
	 * 
	 * @param i
	 *            Zero indexed row position
	 * @param j
	 *            Zero indexed Column position
	 * @param n
	 *            Row size
	 * @return The position in the array
	 */
	public static int getPositionInArray(int i, int j, int n) {
		return ((n * i) + j);
	}

	/**
	 * Called each frame to process all the touch events the MainActivity thread
	 * as given the game thread
	 * 
	 * @param activePieces
	 *            The pieces surrounding the empty slot
	 * @param event
	 *            The touch event
	 * @return A piece if it's being touched
	 */
	public Piece processTouchDownEvent(Piece[] activePieces, MotionEvent event) {
		if (event == null) {
			return null;
		}
		final int size = activePieces.length;
		final float x = event.getRawX();
		final float y = Render.rawScreenHeight - event.getRawY();
		Piece activePiece = null;
		for (int i = 0; i < size; i++) {
			final Piece currentPiece = activePieces[i];
			if (currentPiece != null) {
				final boolean value = Rectangle.pointIntersects(x, y,
						currentPiece.getRect());
				if (value) {
					activePiece = currentPiece;
					break;
				}
			}
		}
		return activePiece;
	}

	/**
	 * 
	 * @param list
	 *            The list of objects to be shuffled
	 * @return Returns the shuffled version of the list
	 */
	public static Piece[] shuffPieces(Piece[] list) {

		final Random random = new Random();
		for (int i = list.length - 1; i > 0; i--) {
			final int swapIndex = random.nextInt(i - 1 + 1) + 1;
			if (list[swapIndex] != null && list[i] != null) {
				swapPiece(list[i], list[swapIndex]);
			}
		}

		final int swapIndex = random.nextInt((list.length - 1) - 1 + 1) + 1;
		swapPiece(freePiece, list[swapIndex]);

		return list;
	}

	public boolean keyPressed(KeyEvent event) {
		/**
		 * If back button pressed, go to pause menu If in pause menu, go to main
		 * menu
		 */
		final int keyCode = event.getKeyCode();
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.d("PicturePuzzle", "Back button pressed");
			moveBack();
		}
		return false;
	}

	/**
	 * Method to handle touch events being sent to this screen
	 */
	public boolean onTouch(MotionEvent event) {

		switch (event.getAction()) {

		case MotionEvent.ACTION_DOWN:
			if (InputManager.vTracker == null) {
				InputManager.vTracker = VelocityTracker.obtain();
			} else {
				InputManager.vTracker.clear();
			}
			InputManager.addVelocityEvent(event);
			if (!pieceSelected) {
				piece = processTouchDownEvent(activePieces, event);
				pieceSelected = true;
			}
			break;
		case MotionEvent.ACTION_UP:
			if (piece != null) {
				final float x = event.getRawX();
				final float y = Render.rawScreenHeight - event.getY();

				if (Rectangle.pointIntersects(x, y, freePiece.getRect())) {
					movePieceTo(piece, freePiece.i, freePiece.j);
				}
			}
			pieceSelected = false;
			piece = null;
			break;
		case MotionEvent.ACTION_MOVE:
			InputManager.addVelocityEvent(event);
			break;
		case MotionEvent.ACTION_CANCEL:
			InputManager.vTracker.recycle();
			break;

		}
		return false;
	}

	/**
	 * Sets up and shows the win game dialog
	 */
	public void showWinGameDialog() {
		DialogFragment winGameDialog = new WinGameDialog();
		final Bundle tempBundle = new Bundle();
		tempBundle.putParcelable("host", this);
		winGameDialog.setArguments(tempBundle);
		winGameDialog.show(getFragmentManager(), "WinGameDialog");
	}

	/**
	 * Listeners for the win game dialog
	 */
	@Override
	public void onNewGameButtonPressed(DialogFragment dialog) {
		destroySelf();
		moveBack();
		Game.screens[1] = null;
		Game.screens[1] = new PicturePuzzle();
		Game.screens[1].init(MainActivity.selfInstance.game,
				MainActivity.selfInstance.render);
		Game.changeScreen(1);
		dialog.dismiss();
	}

	@Override
	public void onExitGameButtonPressed(DialogFragment dialog) {
		moveBack();
	}

	public String toString() {
		return "PicturePuzzle";
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel arg0, int arg1) {		
	}
}
