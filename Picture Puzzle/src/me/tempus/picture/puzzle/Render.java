package me.tempus.picture.puzzle;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import me.tempus.picture.puzzle.R;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

public class Render implements Renderer{

	private static final int DEFAULT_BUFFER_SIZE = 250;
	public static int screenHeight;
	public static int screenWidth;
	public static int rawScreenHeight;
	public static Context context;

	private static MainActivity main;

	private Drawable[] drawables = new Drawable[DEFAULT_BUFFER_SIZE * 2];
	private int bufferIndex = 0;
	private int currentPos = 0;
	private int currentOffSet = 0;

	public static int lastBoundTexture = -1;

	public Render(Context context){
		Render.context = context;
		Render.main = (MainActivity)context;
	}

	public void onDrawFrame(GL10 gl) {
		if(main.game.done){
			return;
		}
		waitThis();
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		final int size = currentPos;
		final int offset = bufferIndex * DEFAULT_BUFFER_SIZE;
		bufferIndex = 1 - bufferIndex;
		currentOffSet = bufferIndex * DEFAULT_BUFFER_SIZE;
		currentPos = 0;
		for(int i = 0; i < size; i++){
			final Drawable d = drawables[i + offset];
			if(d != null){
				d.preDraw(gl);
				d.draw(gl);
				d.postDraw(gl);
			}
			//drawables[i + offset] = null;
		}


	}

	/**
	 * Begins the drawing frame.
	 * Should be called after all the drawables have been added for the current frame
	 */
	public void beginFrame(){
		//this.notifyThis();
		try{
			synchronized (this) {
				this.notify();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param d Drawable to add to list
	 * @return Returns the index in the array, -1 if it wasn't added
	 */
	public int addDrawable(Drawable d){
		if(currentPos < 250){
			drawables[currentPos + currentOffSet] = d;
			currentPos++;
			return (currentPos -1) + currentOffSet;
		}
		return -1;
	}

	/**
	 * A way of a adding a whole list of drawables.
	 * This way there is no way of knowing if one of the drawables didn't get added.
	 * @param d
	 */
	public void addDrawables(Drawable[] d){
		for(Drawable i : d){
			addDrawable(i);
		}
	}

	public void onSurfaceChanged(GL10 gl, int width, int height) {
		gl.glViewport(0, 0, width, height);

		/*
		 * Set our projection matrix. This doesn't have to be done each time we
		 * draw, but usually a new projection needs to be set when the viewport
		 * is resized.
		 */
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glOrthof(0.0f, width, 0.0f, height, 0.0f, 1.0f);

		gl.glShadeModel(GL10.GL_FLAT);
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		gl.glColor4f(0, 0, 0, 1);
		gl.glEnable(GL10.GL_TEXTURE_2D);

		screenHeight = height;
		screenWidth = width;

		loadPicture(gl); // Change these into surface created for EGL context losing?
		loadTextures(gl);
		PicturePuzzle.setUpGrid(PicturePuzzle.rowSize, PicturePuzzle.columnSize, Picture.width, Picture.height); // TODO: Instead of calling this class directly, create an interface that will hold a call back to say it's done loading
		PicturePuzzle.ready = true;
		Game.render = this;
	}

	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		/*
		 * Some one-time OpenGL initialization can be made here probably based
		 * on features of this particular context
		 */

		gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);

		gl.glClearColor(0.66f, 0.69f, 0.71f, 1);
		gl.glShadeModel(GL10.GL_FLAT);
		//gl.glDisable(GL10.GL_DEPTH_TEST);
		gl.glEnable(GL10.GL_TEXTURE_2D);
		/*
		 * By default, OpenGL enables features that improve quality but reduce
		 * performance. One might want to tweak that especially on software
		 * renderer.
		 */
		gl.glDisable(GL10.GL_DITHER);
		gl.glDisable(GL10.GL_LIGHTING);

		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);

	}

	/**
	 * A checked way of binding a texture, just prevents re-binding the same texture 
	 * @param gl GL context
	 * @param target The same as bindTexture's target
	 * @param id ID of the texture
	 */
	public static void bindTexture(GL10 gl, int target, int id){
		if(Render.lastBoundTexture != id){
			//Log.d("Render","TextureId: " + id);
			gl.glBindTexture(target, id);
			lastBoundTexture = id;
		}
	}

	/**
	 * Loads the picture to be used
	 * @param gl
	 */
	public void loadPicture(GL10 gl){
		final Texture picture = Picture.loadBitmap(gl, R.drawable.nintendo_characters_nintendo_512x512);
		Picture.height = picture.height;
		Picture.width = picture.width;
		Picture.id = picture.textureID;
	}
	
	/**
	 * Loads any global textures I'll need
	 * @param gl
	 */
	private void loadTextures(GL10 gl) {
		
		
	}

	/**
	 * A sync and catch way of waiting the thread. Just cleaner code
	 */
	public void waitThis(){
		try{
			synchronized (this) {
				this.wait();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/**
	 * A sync and catch way of notifying the thread. Just cleaner code
	 */
	public void notifyThis(){
		try{
			synchronized (this) {
				this.notify();
				Log.d("Render", "Notified");
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
