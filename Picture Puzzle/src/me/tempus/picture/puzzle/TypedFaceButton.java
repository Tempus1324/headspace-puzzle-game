package me.tempus.picture.puzzle;

import java.util.Hashtable;

import me.tempus.picture.puzzle.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

public class TypedFaceButton extends Button {

	public TypedFaceButton(Context context, AttributeSet attrs) {
		super(context, attrs);		
		
		if(isInEditMode()){
			return;
		}
		
		TypedArray styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.TypedFacedButton);
		String fontName = styledAttributes.getString(R.styleable.TypedFacedButton_typefacefont);
		styledAttributes.recycle();
		
		if(fontName != null){
			Log.d("FontLoading", "Assests: " + context.getAssets() + " Fontname: " + fontName);
			Typeface typeface = TypedFaceButton.getTypeFace(context, fontName);
			setTypeface(typeface);
		}
		
	}
		private static final Hashtable<String, Typeface> cache = new Hashtable<String, Typeface>();

		public static Typeface getTypeFace(Context c, String name){
			synchronized(cache){
				if(!cache.containsKey(name)){
					Typeface t = Typeface.createFromAsset(
							c.getAssets(), 
							name
						);
					cache.put(name, t);
				}
				return cache.get(name);
			}
		}
	}
