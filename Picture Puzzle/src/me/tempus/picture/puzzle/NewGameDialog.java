package me.tempus.picture.puzzle;

import me.tempus.picture.puzzle.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class NewGameDialog extends DialogFragment {
	public interface NewGameListener {
		public void onNewGameButtonPressed(DialogFragment dialog);

		public void onExitGameButtonPressed(DialogFragment dialog);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final DialogFragment self = this; // Work around for accessing within
											// listeners
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();

		View dialogView = inflater.inflate(R.layout.newgamedialog, null);

		final Button easyButton = (Button) dialogView
				.findViewById(R.id.newgameEasyButton);
		final Button mediumButton = (Button) dialogView
				.findViewById(R.id.newgameMediumButton);
		final Button hardButton = (Button) dialogView
				.findViewById(R.id.newgameHardButton);

		easyButton.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				final int action = event.getAction();
				switch (action) {

				case MotionEvent.ACTION_DOWN:
					break;
				case MotionEvent.ACTION_UP:
					break;

				}

				return false;
			}
		});
		easyButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Game.screens[1] = null;
				PicturePuzzle.rowSize = 3;
				PicturePuzzle.columnSize = 3;
				Game.screens[1] = new PicturePuzzle();
				Game.screens[1].init(MainActivity.selfInstance.game,
						MainActivity.selfInstance.render);
				Game.changeScreen(1);
				self.dismiss();
			}
		});

		mediumButton.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				final int action = event.getAction();
				switch (action) {

				case MotionEvent.ACTION_DOWN:
					break;
				case MotionEvent.ACTION_UP:
					break;

				}

				return false;
			}
		});
		mediumButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Game.screens[1] = null;
				PicturePuzzle.rowSize = 4;
				PicturePuzzle.columnSize = 4;
				Game.screens[1] = new PicturePuzzle();
				Game.screens[1].init(MainActivity.selfInstance.game,
						MainActivity.selfInstance.render);
				Game.changeScreen(1);
				self.dismiss();
			}
		});

		hardButton.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				final int action = event.getAction();
				switch (action) {

				case MotionEvent.ACTION_DOWN:
					break;
				case MotionEvent.ACTION_UP:
					break;

				}

				return false;
			}
		});
		hardButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				Game.screens[1] = null;
				PicturePuzzle.rowSize = 6;
				PicturePuzzle.columnSize = 6;
				Game.screens[1] = new PicturePuzzle();
				Game.screens[1].init(MainActivity.selfInstance.game,
						MainActivity.selfInstance.render);
				Game.changeScreen(1);
				self.dismiss();
			}
		});

		builder.setView(dialogView);

		return builder.create();
	}
}
