package me.tempus.picture.puzzle;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLUtils;
import android.util.Log;

public class Util {

	/**
	 * Loads a texture
	 * @param gl The OpenGL context
	 * @param resourceId The resource ID according to the generated resource IDs (tested against png)
	 * @return Texture ID
	 */
	private static BitmapFactory.Options sBitmapOptions = new BitmapFactory.Options(); //Used in texture loading
	public static int loadBitmap(GL10 gl, int resourceId) { // Texture Name
		int height;
		int width;

		sBitmapOptions.inPreferredConfig = Bitmap.Config.RGB_565;
		int textureName = -1;
		int[] textureNames = new int[1];
		Context context = Render.context;
		if (context != null && gl != null) {
			gl.glGenTextures(1, textureNames, 0);

			textureName = textureNames[0];
			gl.glBindTexture(GL10.GL_TEXTURE_2D, textureName);

			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);

			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_CLAMP_TO_EDGE);
			gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_CLAMP_TO_EDGE);

			gl.glTexEnvf(GL10.GL_TEXTURE_ENV, GL10.GL_TEXTURE_ENV_MODE, GL10.GL_REPLACE);

			InputStream is = context.getResources().openRawResource(resourceId);
			Bitmap bitmap;

			try {
				bitmap = BitmapFactory.decodeStream(is, null, sBitmapOptions);
				height = bitmap.getHeight();
				width = bitmap.getWidth();
			} finally {
				try {
					is.close();
				} catch (IOException e) {
				}
			}

			GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

			bitmap.recycle();
			((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, 
					GL11Ext.GL_TEXTURE_CROP_RECT_OES, new int[]{0, height, width, -height}, 0);

			int error = gl.glGetError();
			if (error != GL10.GL_NO_ERROR) {
				Log.e("TextureLoading", "Texture Load GLError: " + error);
			}

		}
		//id = textureName;
		return textureName;
	}
}
