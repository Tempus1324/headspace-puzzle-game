package me.tempus.picture.puzzle;

import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;

public class InputManager {

	/**
	 * It should be that the update thread pings this to process the input events that has been stored here
	 * As it goes through each event/keypress/etc it tells the currentScren in the game
	 */
	
	public static final FixedArray<MotionEvent> events = new FixedArray<MotionEvent>(30);
	public static VelocityTracker vTracker;
	
	public static final FixedArray<KeyEvent> keyEvents = new FixedArray<KeyEvent>(15);
	
	public static void processInput(Game game, Render render){
		/**
		 * TODO Could be optimized
		 * Put it all in one loop, it will reduced iterations and checks
		 */
		final int size = events.contentIndex+1;
		final Object[] eventsContents = events.getContents();
		for(int i = 0; i < size; i++){
			final MotionEvent event = (MotionEvent)eventsContents[i];
			if(event != null){
				Game.currentScreen.onTouch(event);
				events.remove(i, true);
			}
		}
		events.reset();
		final int keySize = keyEvents.contentIndex+1;
		final Object[] keyEventContents = keyEvents.getContents();
		for(int i = 0; i < keySize; i++){
			final KeyEvent event = (KeyEvent)keyEventContents[i];
			if(event != null){
				Game.currentScreen.keyPressed(event);
				keyEvents.remove(i, true);
			}
		}
		keyEvents.reset();
	}
	
	
	/**
	 * Adds a motion even to the list, if there are too many events, it will not add
	 * @param v
	 * @param event
	 */
	public static void addMotionEvent(View v, MotionEvent event){
		if(event.getAction() == MotionEvent.ACTION_MOVE){
			return;
		}
		events.add(event);
	}
	
	/**
	 * Add keyEvent to key events list
	 * @param v View
	 * @param keyCode The key code
	 * @param event The actual event
	 */
	public static void addKeyEvent(View v, int keyCode, KeyEvent event){
		
		keyEvents.add(event);
	}
	
	/**
	 * Adds a event to the Velocity, basically redundant and pointless
	 * @param event
	 */
	//@Deprecated
	public static void addVelocityEvent(MotionEvent event){
		if(event != null)
			vTracker.addMovement(event);
	}
	
	public static void frameDone(){
		
	}
}
