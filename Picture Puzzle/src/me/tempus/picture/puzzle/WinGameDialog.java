package me.tempus.picture.puzzle;

import me.tempus.picture.puzzle.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;

public class WinGameDialog extends DialogFragment {

	private Fragment host;
	
	public interface WinGameListener {
		public void onNewGameButtonPressed(DialogFragment dialog);

		public void onExitGameButtonPressed(DialogFragment dialog);
	}

	WinGameListener listener;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		host = (Fragment) getArguments().get("host");
		try {
			listener = (WinGameListener) host;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement new game listener!");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final DialogFragment self = this; // Work around for accessing within listeners
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		LayoutInflater inflater = getActivity().getLayoutInflater();

		View dialogView = inflater.inflate(R.layout.wingamedialog, null);

		final Button newGameButton = (Button) dialogView
				.findViewById(R.id.windialogNewGameButton);
		final Button exitGameButton = (Button) dialogView
				.findViewById(R.id.windialogExitButton);

		newGameButton.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				final int action = event.getAction();
				switch (action) {

				case MotionEvent.ACTION_DOWN:
					break;
				case MotionEvent.ACTION_UP:
					break;

				}

				return false;
			}
		});
		newGameButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				listener.onNewGameButtonPressed(self);
			}
		});
		
		exitGameButton.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View view, MotionEvent event) {

				final int action = event.getAction();
				switch (action) {

				case MotionEvent.ACTION_DOWN:
					break;
				case MotionEvent.ACTION_UP:
					break;

				}

				return false;
			}
		});
		exitGameButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				listener.onExitGameButtonPressed(self);
				self.dismiss();
			}
		});
		builder.setView(dialogView);

		return builder.create();
	}

}
