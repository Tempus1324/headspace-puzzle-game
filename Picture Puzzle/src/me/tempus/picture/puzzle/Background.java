/**
 * 
 */
package me.tempus.picture.puzzle;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11Ext;

/**
 * @author Chris
 *
 */
public class Background implements Drawable {
	
	public int width;
	public int height;
	public int textureID;

	/* (non-Javadoc)
	 * @see me.tempus.headspace.picture.puzzle.Drawable#preDraw(javax.microedition.khronos.opengles.GL10)
	 */
	public void preDraw(GL10 gl) {
	}

	/* (non-Javadoc)
	 * @see me.tempus.headspace.picture.puzzle.Drawable#draw(javax.microedition.khronos.opengles.GL10)
	 */
	public void draw(GL10 gl) {
		Render.bindTexture(gl, GL10.GL_TEXTURE_2D, textureID);
		
		((GL11Ext) gl).glDrawTexfOES(0, 0, 0, width, height);
	}

	/* (non-Javadoc)
	 * @see me.tempus.headspace.picture.puzzle.Drawable#postDraw(javax.microedition.khronos.opengles.GL10)
	 */
	public void postDraw(GL10 gl) {
	}

}
