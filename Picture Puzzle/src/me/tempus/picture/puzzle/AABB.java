package me.tempus.picture.puzzle;

public interface AABB {

	public abstract Rectangle getRect();
}
