/**
 * 
 */
package me.tempus.picture.puzzle;

import me.tempus.picture.puzzle.R;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;

import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

/**
 * @author Chris
 *
 */
public class MainMenu extends Fragment implements Screen {

	public static int buttonStartText;
	public static int buttonAboutText;

	public static Button buttonStart;
	public static Button buttonAbout;

	public final static Background background = new Background();
	
	public static int buttonAboutTextPressed;
	public static int buttonStartTextPressed;

	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		
		View field1 = inflater.inflate(R.layout.mainmenu, container, false);
		final Button enterGameButton = (Button)field1.findViewById(R.id.enterGameButton);
		
		Log.d("MainMenu", "EnterGameButton colour: " + enterGameButton.getBackground());
		
		enterGameButton.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View view, MotionEvent event) {				
				final int action = event.getAction();
				switch (action){
				
				case MotionEvent.ACTION_DOWN:
					enterGameButton.setBackgroundResource(R.drawable.mainmenu_buttonbackground_gradient_off);
					break;
				case MotionEvent.ACTION_UP:
					enterGameButton.setBackgroundResource(R.drawable.mainmenu_buttonbackground_gradient_on);
					break;
				
				}
				
				return false;
			}
		});
		enterGameButton.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				DialogFragment newGameDialog = new NewGameDialog();
				final Bundle tempBundle = new Bundle();
				newGameDialog.setArguments(tempBundle);
				newGameDialog.show(getFragmentManager(), "WinGameDialog");
			}
		});
		
		final Button exitButton = (Button)field1.findViewById(R.id.quitButton);
		
		exitButton.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				
				final int action = event.getAction();
				switch (action){
				
				case MotionEvent.ACTION_DOWN:
					exitButton.setBackgroundResource(R.drawable.mainmenu_buttonbackground_gradient_off);
					break;
				case MotionEvent.ACTION_UP:
					exitButton.setBackgroundResource(R.drawable.mainmenu_buttonbackground_gradient_on);
					break;
				
				}
				
				return false;
			}
		});
		
		exitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				exitButton.setBackgroundResource(R.drawable.mainmenu_buttonbackground_gradient_off);
				System.exit(0);
			}
		});
        return field1;
    }
	
	@Override
	public void onResume(){
		final View view = getView();
		
		View logoView = view.findViewById(R.id.headSpaceLogo);
		View buttons = view.findViewById(R.id.mainMenuButtons);
		
		final int initialPos = logoView.getTop();
		ObjectAnimator.ofFloat(logoView, "translationY", Render.rawScreenHeight).setDuration(0).start();
		ObjectAnimator.ofFloat(buttons, "alpha", 0).setDuration(0).start();
		
		AnimatorSet set = new AnimatorSet();
		set.playSequentially(
				ObjectAnimator.ofFloat(logoView, "translationY", initialPos).setDuration(1000),
				ObjectAnimator.ofFloat(buttons, "alpha", 1).setDuration(1000)
				);
		
		set.start();
		
		super.onResume();
	}
	
	public void preUpdate(Game game, Render render) {

	}

	
	public void update(Game game, Render render) {

	}

	public void pause(Game game) {

	}

	public void init(final Game game, final Render render) {
		background.height = Render.rawScreenHeight;
		background.width = Render.screenWidth;

	}

	public String toString(){
		return "MainMenu";
	}
	
	public void resume(Game game, Render render) {

	}

	public void stop(Game game) {

	}

	public boolean keyPressed(KeyEvent event) {

		return false;
	}

	public boolean onTouch(MotionEvent event) {
		
		return false;
	}
}
