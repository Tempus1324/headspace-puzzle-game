package me.tempus.picture.puzzle;

import me.tempus.picture.puzzle.R;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnTouchListener;

public class MainActivity extends FragmentActivity implements OnKeyListener, OnTouchListener, OnClickListener {

	public static MainActivity selfInstance;
	
	public Render render;
	public Game game;
	
	public FragmentManager fragManager;
	public FragmentTransaction fragTranscation;
	
	private int threadsLoaded = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		selfInstance = this;
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.main);
		
		fragManager = getSupportFragmentManager();
		
		game = new Game(this, render);
		(new Thread(game)).start();
		
		Render.rawScreenHeight = getWindowManager().getDefaultDisplay().getHeight();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	/**
	 * Called when either the render or the game thread is done loading.  Makes sure they don't start operating on each other before they're done loading
	 */
	public void threadLoaded(){
		threadsLoaded++;
		if(threadsLoaded >= 2){
			game.notifyThis();
			render.notifyThis();
		}
	}

	/** Remember to resume the glSurface  */
	@Override
	protected void onResume() {
		
		game.resume();
		super.onResume();
	}

	/** Also pause the glSurface  */
	@Override
	protected void onPause() {
		game.pause();
		super.onPause();		
	}

	public void onClick(View arg0) {
	}

	public boolean onTouch(View v, MotionEvent event) {
		try{
			InputManager.addMotionEvent(v, event);
			synchronized (this) {
				wait(16);
			}

		}catch(InterruptedException e){
			e.printStackTrace();
		}
		return false;
	}

	public boolean onKeyDown(int keyCode, KeyEvent event){
		if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
			// do something on back.
			InputManager.addKeyEvent(null, keyCode, event);
			//return true;
		}

		return true;	
	}
	public boolean onKey(View v, int keyCode, KeyEvent event) {
		try{
			InputManager.addKeyEvent(v, keyCode, event);
			synchronized (this) {
				wait(16);
			}
		}catch(InterruptedException e){
			e.printStackTrace();
		}
		return false;
	}


}
