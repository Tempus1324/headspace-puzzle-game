package me.tempus.picture.puzzle;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;
import javax.microedition.khronos.opengles.GL11Ext;

public class Piece implements AABB, Drawable {

	//Global data
	public static int pieceWidth;
	public static int pieceHeight;
	public static int xPadding;
	public static int yPadding;
	public static int textureID;


	//Piece specific
	public int i;
	public int j;
	public final int initI;
	public final int initJ;
	private final Rectangle rect;
	private final int[] textureRect;

	public Piece(int i, int j, int[] textureRect) {
		this.initI = i;
		this.initJ = j;
		this.i = i;
		this.j = j;
		this.rect = new Rectangle(i * pieceWidth, j * pieceHeight, pieceWidth, pieceHeight);
		this.textureRect = textureRect;  
	}
	public Rectangle getRect() {
		/**
		 * Should return a new rectangle
		 * Position should be derived off it's position in the grid.
		 * x = i * pieceWidth
		 * y = j * pieceHeight
		 */
		rect.x = i * pieceWidth;
		rect.y = j * pieceHeight;
		return rect;
	}

	public void preDraw(GL10 gl) {

	}

	public void draw(GL10 gl) {
		Render.bindTexture(gl, GL10.GL_TEXTURE_2D, Picture.id);
		//TODO Render the piece here

		if(textureRect != null){
		((GL11) gl).glTexParameteriv(GL10.GL_TEXTURE_2D, 
				GL11Ext.GL_TEXTURE_CROP_RECT_OES, textureRect, 0);
		}
		((GL11Ext) gl).glDrawTexfOES((i * pieceWidth), (j * pieceHeight), 0, pieceWidth - xPadding, pieceHeight - yPadding);
	}
	
	public void postDraw(GL10 gl) {

	}

	@Override
	public String toString(){
		return "(" + i + ", " + j + ")" + " Width, Height: " + pieceHeight + ", " + pieceWidth + "(" + i*pieceWidth + ", " + j*pieceHeight + ")";
	}


}
